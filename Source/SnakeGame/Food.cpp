// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			AFood::Destroy();
			AFood::AddRandomFood();
		}
	}
}

void AFood::AddRandomFood()
{
	BP_Food = LoadObject<UBlueprint>(nullptr, TEXT("/Script/Engine.Blueprint'/Game/Blueprints/GameModes/BP_Foods.BP_Foods'"));
	TSubclassOf<class UObject> blockBP = (UClass*)BP_Food->GeneratedClass;

	FRotator StartPointRotation = FRotator(0, 0, 0);

	float Spawn_X = FMath::FRandRange(min_X, max_X);
	float Spawn_Y = FMath::FRandRange(min_Y, max_Y);

	FVector FoodSpawnPoint = FVector(Spawn_X, Spawn_Y, -80.f);

	if (IsValid(GetWorld()))
	{
		GetWorld()->SpawnActor<AActor>(blockBP ,FoodSpawnPoint, StartPointRotation);
	}
}


